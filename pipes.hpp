#ifndef PIPES_H
#define PIPES_H

#include <iostream>
#include <sys/wait.h>
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <vector>

void pipeIt(std::vector<std::string> cmdlist);
void initialFork(std::vector<std::string> cmdlist);
void recursivePipes(std::vector<std::vector<std::string> > cmdlist, int position, int fd);
void iterativePipes(std::vector<std::vector<std::string> > cmdlist);
std::vector<std::vector<std::string> > convertCmdList(std::vector<std::string> cmdlist);

#endif 