#ifndef FORK_H
#define FORK_H

#include <string>
#include <vector>
#include <unistd.h>
#include <cstdlib>
#include <errno.h>
#include <iostream> 
#include <cstring> 
#include <sys/wait.h>

void handleErrors(); //ERRNO is a global variable don't need a parameter. 
void forkIntoProcess(std::vector<std::string> commandList);

#endif
