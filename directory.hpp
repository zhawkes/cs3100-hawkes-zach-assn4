#ifndef DIRECTORY_H
#define DIRECTORY_H
#include <string>
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <iostream> 

void changeDir(std::string inputPath);
void showDir();

#endif