#include <iostream>
#include <string> 
#include <vector>
#include <sstream> 
#include <iterator>
#include <csignal>  
#include "fork.hpp"
#include "directory.hpp"
#include "pipes.hpp"
#include <chrono>


bool quittingTime = false;

//parse multiWord string inputted by user
std::vector<std::string> parseCommand(std::string commands){ 
  std::istringstream ss(commands);
  std::istream_iterator<std::string> begin(ss), end;
  std::vector<std::string> cmdList(begin, end); 
  return cmdList; 
}

void signalHandler(int __attribute__((unused))  notUsed){
  quittingTime = true; 
}

void displayHistory(std::vector<std::string> history){
  std::cout << "---Command History---" <<std::endl;
  std::cout << std::endl;
  for(unsigned int i = 0; i < history.size(); i++){
    std::cout << i + 1<< ": " << history[i] << std::endl;
  }
  std::cout << std::endl;
}

void menu(){
  bool runProgram = true;
  double ptime = 0.0;
  std::vector<std::string> history;
  while(runProgram){
    std::cout << "[cmd] ";
    std::string commandString; 
    std::getline(std::cin, commandString);
    if(commandString != ""){
      history.push_back(commandString);
      std::vector<std::string> cmdList = parseCommand(commandString);
      
      if(cmdList[0] == "exit") exit(0); // handle exit

      if(cmdList[0] == "history"){
        displayHistory(history);

      } else if (cmdList[0] == "ptime") { //ptime command
        if(ptime != 0.0){
          std::cout << "Execution time of last command: " << ptime << " seconds"<< std::endl;
        } else {
          std::cout << "No previous commands" << std::endl;
        }

      } else if (cmdList[0] == "^" || cmdList[0][0] == '^'){ //use history command
        if (cmdList.size() > 1){
          int index = std::stoi(cmdList[1]);
          std::vector<std::string> newCommand = parseCommand(history[index - 1]);
          auto before = std::chrono::high_resolution_clock::now();
          forkIntoProcess(newCommand);
          auto after = std::chrono::high_resolution_clock::now();
          std::chrono::duration<double> dur = after - before;
          ptime = dur.count();
          history.push_back(history[index - 1]);
        } else {
          std::cout << "Not enough arguments! (Must specify number of commmand from history, or put space between '^' and number)" << std::endl;
        }

      } else if(cmdList[0] == "pwd"){ // print working directory command
        showDir();

      } else if(cmdList[0] == "cd"){ // change directory command 
        changeDir(cmdList[1]);

      } else { // dealing with pipes
        bool forkIt = true;
        for(unsigned int i = 0; i < cmdList.size(); i++){
          if(cmdList[i] == "|"){
            pipeIt(cmdList);   
            // initialFork(cmdList);
            forkIt = false;
            break;
          }
        }
        if(forkIt){
          //normal command
          auto before = std::chrono::high_resolution_clock::now();
          forkIntoProcess(cmdList);
          auto after = std::chrono::high_resolution_clock::now();
          std::chrono::duration<double> dur = after - before;
          ptime = dur.count();
        }
      } 
    }
  }
}

int main(void){
  std::signal(SIGINT, signalHandler);
  std::cout << "This shell currently only supports two piped commands" << std::endl;
  menu();
  
  return 0;
}
