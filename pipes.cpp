#include "pipes.hpp"
#include "fork.hpp"

const int READ = 0;
const int WRITE = 1;


//print vector is here for debugging purposes. 
void printVector(std::vector<std::vector<std::string> > commands){
    std::cout << "Printing Command Vector\n";
    for(unsigned int i = 0; i < commands.size(); i++){
        for(unsigned int j = 0; j < commands[i].size(); j++){
            std::cout << commands[i][j] << std::endl;
        }
        std::cout << "------------" << std::endl;
    }
    std::cout << std::endl;
}

std::vector<std::vector<std::string> > convertCmdList(std::vector<std::string> cmdlist){
    std::vector<std::vector<std::string> > returnValue;
    std::vector<std::string> pushToVector;
    for(unsigned int i = 0; i < cmdlist.size(); i++){
        if(cmdlist[i] == "|"){
            returnValue.push_back(pushToVector);
            pushToVector.clear();
        } else {
            pushToVector.push_back(cmdlist[i]);
        }
    }
    returnValue.push_back(pushToVector);
    // printVector(returnValue);
    return returnValue;
}

void pipeIt(std::vector<std::string> cmdlist){
    std::vector<std::vector<std::string> > commands = convertCmdList(cmdlist);

    int p[2];
    if(pipe(p) != 0){
        std::cout << "there was an error.\n";
    }

    pid_t first = fork();
    if(first == 0){
        close(p[READ]);
        dup2(p[WRITE], STDOUT_FILENO);
        char* cmd[commands[0].size() + 1];
        for(unsigned int j = 0; j < commands[0].size(); j++){
            cmd[j] = strdup(commands[0][j].c_str());
        }
        cmd[commands[0].size()] = (char*)NULL;
        execvp(cmd[0], cmd);
    }

    pid_t second = fork();
    if(second == 0){
        close(p[WRITE]);
        dup2(p[READ], STDIN_FILENO);
        char* cmd[commands[1].size() + 1];
        for(unsigned int j = 0; j < commands[1].size(); j++){
            cmd[j] = strdup(commands[1][j].c_str());
        }
        cmd[commands[1].size()] = (char*)NULL;
        execvp(cmd[0], cmd);
    }
    close(p[READ]);
    close(p[WRITE]);
    
    int wstatus;
    for (int i = 0; i < 2; ++i) {
        pid_t kiddo = wait(&wstatus);
        if (kiddo == first) {
        //     std::cerr << "The `first` process terminated with status " << WEXITSTATUS(wstatus) << std::endl;
         }
        else if (kiddo == second) {
        //     std::cerr << "The `second` process terminated with status " << WEXITSTATUS(wstatus) << std::endl;
         }
    }
}

/*IGNORE EVERYTHING BELOW THIS FOR THIS COMMIT, IT IS WORK IN PROGRESS, BROKEN CODE
 * Would be Happy to take partial credit for it though. :)
 *
*/

/******************

void initialFork(std::vector<std::string> cmdlist){
    std::vector<std::vector<std::string> > commands = convertCmdList(cmdlist);
    // recursivePipes(commands, 0, STDIN_FILENO); 
    iterativePipes(commands);
}
void recursivePipes(std::vector<std::vector<std::string> > cmdlist, int position, int fd){
    if(position >= cmdlist.size() - 1){
        if(fd != STDIN_FILENO){
            if(dup2(fd, STDIN_FILENO) != -1){
                close(fd);
            }
        }
        char* cmd[cmdlist[position].size() + 1];
        for(unsigned int i = 0; i < cmdlist[position].size(); i++){
            cmd[i] = strdup(cmdlist[position][i].c_str());
        }
        cmd[cmdlist[position].size()] = (char*)NULL;
        // std::cout << "Last Command is " << cmd[0] << std::endl;
        int value = execvp(cmd[0], cmd);
        std::cout << "Handling errors base case\n";
        handleErrors();
    } else {
        int p[2];
        pid_t child; 
        child = fork();
        if(pipe(p) != 0){
            std::cout << "Couldn't PIPE!\n";
            handleErrors();
        }    
        if(child == 0){
            close(p[READ]);
            if(dup2(fd, STDIN_FILENO) == -1) std::cout << "Recursive dup (1) error\n";
            if(dup2(p[WRITE], STDOUT_FILENO) == -1){ 
                std::cout << "Recursive dup (2) error\n";
                handleErrors();
            }
            char* cmd[cmdlist[position].size() + 1];
            for(unsigned int i = 0; i < cmdlist[position].size(); i++){
                cmd[i] = strdup(cmdlist[position][i].c_str());
            }
            cmd[cmdlist[position].size()] = (char*)NULL;
            // std::cout << "This is a command " << cmd[0] << std::endl;
            int value = execvp(cmd[0], cmd);
            handleErrors();
        } 
        close(p[WRITE]);
        close(fd);
        recursivePipes(cmdlist, position + 1, p[READ]);
    }
}

// void recursivePipes2(std::vector<std::vector<std::string> > cmdlist, int position, int fd){
//     if(position == 0){
        
//     } else {
        
//     }
// }
void iterativePipes(std::vector<std::vector<std::string> > cmdlist){
    pid_t kids[cmdlist.size()];
    int p[2];
    pipe(p);
    bool start = true;
    for(int i = cmdlist.size(); i > 0; i--){
        kids[i] = fork();
        if(kids[i] == 0){
            std::cout << "Yay made it here " << getpid() << std::endl;
            if(!start){ 
                dup2(p[READ], STDIN_FILENO);
            } else {
                close(p[READ]);
            }
            if(i == 1){
                dup2(p[WRITE], STDOUT_FILENO);
            } else {
                close(p[WRITE]);
            }
            char* cmd[cmdlist[i].size() + 1];
            for(unsigned int j = 0; j < cmdlist[i].size(); j++){
                cmd[i] = strdup(cmdlist[i][j].c_str());
            }
            cmd[cmdlist[i].size()] = (char*)NULL;
            execvp(cmd[0], cmd);
            handleErrors();
        }
            start = false;
    }
    close(p[READ]);
    close(p[WRITE]);
}

*/
/*
How to do this with an arbitrarily amount of forks? For loop? 
Could it potentially be done with actual recursion? 
baseCase {if(cmdlistIsEmpty) stop recursing}
fork
    fork
        fork
        (close)pipe(write)
    (read)pipe(write)
(read)pipe(close)
 */
