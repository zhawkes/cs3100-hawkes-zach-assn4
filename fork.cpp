#include "fork.hpp"


void handleErrors(){
  std::string error = strerror(errno);
  std::cout << error << std::endl;
}


void forkIntoProcess(std::vector<std::string> commandList){  
  pid_t child = fork(); 
  if(child != 0){
    wait(NULL);
  } else if (child == 0) {
    char* cmd[commandList.size() + 1];
    for(unsigned int i = 0; i < commandList.size(); i++){
        cmd[i] = strdup(commandList[i].c_str());
    }
    cmd[commandList.size()] = (char*)NULL;
    execvp(cmd[0], cmd);
    handleErrors();
    exit(0);
  }  
}
