#include "directory.hpp"

void changeDir(std::string inputPath){
    char* path = new char [2];
    strcpy(path, inputPath.c_str());
    chdir(path);
    showDir();
}

void showDir(){
    char* buffer = (char*) malloc(128);
    char* result;
    size_t size = 128;
    result = getcwd(buffer, size);
    std::cout << result << std::endl;
    free(buffer);
}
